#include <iostream>
template <typename T>
class Stack
{
private:
    int CurrElem;
    int StackSize;
    T* p;
   
public:
    Stack()
    {
        p = new T[10];
        StackSize = 10;
        CurrElem = -1;
    }

    Stack(int x)
    {
        p = new T[x];
        CurrElem = -1;
        StackSize = x;
    }

    ~Stack()
    {
        delete p;
    }

   void Pop()
    {
       if (CurrElem == -1)
       {
           std::cout << "Stack is empty\n";
       }
       else
       {
           p[CurrElem] = 0;
           CurrElem--;
       }
      
       


    }
   
   void Push(T a)
   {
   
       if (CurrElem == StackSize - 1)
       {
           std::cout << "Stack is full\n";
       }
       else
       {
           CurrElem++;
           p[CurrElem] = a;
       }
       
   
   }

   void StackInfo()
   {
       if (CurrElem == -1)
       {
           std::cout << "Stack contain:" << CurrElem + 1 << " elements\n";
       }
       else
       { 

       std::cout << "Stack contain:" << CurrElem + 1 << " elements\n";

        for (int i = 0; i <= CurrElem; i++)
            {
            std::cout << "Element " << i << " = " << *(p + i) << "\n";
            }
       }
   }

};

int main()
{

    Stack<int> temp(4);
    temp.Push(3);
    temp.Pop();
    temp.StackInfo();
    temp.Pop();

    temp.Push(3);
    temp.Push(8);
    temp.StackInfo();

    std::cout << "_______________________________________\n";
    
    Stack<char> temp1(5);
    temp1.Push('a');
    temp1.Push('b');
    temp1.Push('c');
    temp1.Push('h');
    temp1.Push('j');
    temp1.Push('k');
    temp1.StackInfo();
}